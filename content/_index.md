---
title: Home
draft: false
weight: 10
bannerHeading: Grant Services
bannerText: Let CCRPC help you find and administer funding for your project.
bannerUrl: banner.jpg
---

# Grant Services

For over 50 years, CCRPC has collaborated with local governments and agencies to maximize opportunities
for area residents, currently working with communities in multiple counties in east central Illinois. As an agency
administering over 140 federal and state grants and contracts, CCRPC has staff with substantial knowledge
in regional, transportation, and environmental planning; and economic, community, and workforce development
who are ready to assist your needs.

Our team can help you with a range of grant-related tasks:
- Research grant opportunities
- Write full grant proposals and applications
- Develop project scope of work and budget details
- Coordinate grant applications with engineering firms and other contractors
- Administer grants

For more information or to request a cost estimate for services, contact Susan Ebert at sebert@ccrpc.org or call 217-819-4077.

-----------------

# Recent Grant Awards

### 2024
- **$380,022** awarded to CCRPC by the Federal Highway Administration (FHWA) through its <a href="https://www.fhwa.dot.gov/environment/protect/discretionary/">Promoting Resilient Operations for Transformative, Efficient, and Cost-Saving Transportation (PROTECT)</a> grant program to study transportation infrastructure vulnerability during severe weather events throughout Champaign County.

### 2023
- **$1,466,040** awarded to the City of Urbana by the <a href="https://idot.illinois.gov/transportation-system/local-transportation-partners/county-engineers-and-local-public-agencies/funding-programs/itep.html">Illinois Transportation Enhancement Program (ITEP)</a> grant to construct a shared-use path on Bakers Lane between Washington Street and Main Street.
- **$1,098,070** awarded to the City of Urbana by the <a href="https://idot.illinois.gov/transportation-system/local-transportation-partners/county-engineers-and-local-public-agencies/funding-programs/itep.html">Illinois Transportation Enhancement Program (ITEP)</a> grant to construct a shared-use path along Florida Avenue from Lincoln Avenue to Race Street.
- **$945,000** awarded to CCRPC by the <a href="https://www.transportation.gov/grants/SS4A">Safe Streets for All (SS4A)</a> program to update safety data used in transportation plans, and conduct a demonstration project on Lincoln Avenue in Urbana.
- **$455,706** awarded to CCRPC by the <a href="https://idot.illinois.gov/transportation-system/transportation-management/planning.html">IDOT State Planning and Research (SPR)</a> program to update the CUUATS modeling suite for use in transportation planning processes.
- **$404.048** awarded to CCRPC by the <a href="https://idot.illinois.gov/transportation-system/transportation-management/planning.html">IDOT State Planning and Research (SPR)</a> program to create the Housing and Transportation Affordability and Accessibility Index Neighborhood Toolkit.

### 2022
- **$234,900** awarded to the Village of Fisher by the <a href="https://dnr.illinois.gov/grants/openspacelandsaquisitiondevelopment-grant.html">IDNR Open Space Land Acquisition and Development (OSLAD)</a> grant to renovate its park playground and add new park features.
- **$171,372** awarded to CCRPC by the <a href="https://idot.illinois.gov/transportation-system/local-transportation-partners/public-transportation-providers/technical-studies.html">IDOT Office of Intermodal Project Implementation (OIPI)</a> to develop new tools for looking deeper into the collective needs of the community in order to better serve all individuals, particularly those that are underrepresented, disadvantaged, and economically distressed. The outcome of the project will include a report on transportation equity in the MPO region as well as a new tool for extracting Census data to use in QGIS.
- **$110,113** awarded to CCRPC by the <a href="https://idot.illinois.gov/transportation-system/local-transportation-partners/public-transportation-providers/technical-studies.html">IDOT Office of Intermodal Project Implementation (OIPI)</a> to evaluate Champaign-Urbana Mass Transit District routes using the CCRPC Sustainable Neighborhoods Toolkit.
- **$112,101** awarded to CCRPC by the <a href="https://idot.illinois.gov/transportation-system/local-transportation-partners/public-transportation-providers/technical-studies.html">IDOT Office of Intermodal Project Implementation (OIPI)</a> to evaluate transportation costs and inequities in the transit system connecting the Village of Rantoul with the Champaign-Urbana area.
- **$299,376** awarded to CCRPC by the IDOT Office of Planning and Programming to complete a corridor study on Lincoln Avenue from Green Street to Florida Avenue in Urbana.

### 2021
- **$25,000** awarded to Sagamore Publishing, LLC by the Community Development Block Grant (CDBG) Downstate Small Business Stabilization program to provide working capital.

### 2020
- **$550,000** awarded to the Village of Bradley by the Illinois Department of Commerce & Economic Opportunity (DCEO) Community Development Block Grant (CDBG) for a Housing Rehabilitation program to rehabiliate ten homes in the village.
- **$2,553,900** awarded to Champaign County by the <a href="https://idot.illinois.gov/transportation-system/local-transportation-partners/county-engineers-and-local-public-agencies/funding-programs/hsip.html">IDOT Highway Safety Improvement Program (HSIP)</a> for safety improvements on County Highway 18 (CR 900N) in Champaign County.
