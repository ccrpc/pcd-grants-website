---
title: Funding Opportunities
draft: false
menu: main
weight: 20
summary: This page provides information about types of grant applications our staff have worked on.
bannerHeading: Funding Opportunities
bannerText: This page provides information about some of the funding applications our staff have worked on. Contact us with any of your project ideas! 
---

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="idot-itep-program">1. IDOT Transportation Enhancement Program (ITEP)</span>

**Description:** ITEP provides funding for community-based projects that expand travel choices and enhance the transportation experience by improving the cultural, historic, aesthetic, and environmental aspects of our transportation infrastructure.

**Website:** https://idot.illinois.gov/transportation-system/local-transportation-partners/county-engineers-and-local-public-agencies/funding-programs/itep.html

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="idot-statewide-planning-and-research">2. IDOT Statewide Planning and Research (SPR)</span>

**Description:** Statewide Planning & Research (SPR) funds are used to support planning and research activities. The funds are used to establish a cooperative, continuous, and comprehensive framework for making transportation investment decisions and to carry out transportation planning and research activities throughout the State.

**Website:** https://idot.illinois.gov/transportation-system/transportation-management/planning.html

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="idot-oipi-technical-studies-program">3. IDOT OIPI Technical Studies Program</span>

**Description:** IDOT’s Office of Intermodal Project Implementation (OIPI) funds and works with eligible Grantees on public transit and intermodal transportation planning studies. These studies cover a wide range of activities that are eligible under 49 USC §5305. They are designed to help Grantees make more informed choices when later seeking capital or operations funding.

**Website:** https://idot.illinois.gov/transportation-system/local-transportation-partners/public-transportation-providers/technical-studies.html

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="highway-safety-improvement-program">4. Highway Safety Improvement Program (HSIP)</span>

**Description:** Illinois’ HSIP is a core federal program (Section 148 of Title 23, United States Code) intended to produce a measurable and significant reduction in fatalities and serious injuries resulting from traffic-related crashes on all public roads. All phases of a safety improvement project are eligible for this program, including preliminary engineering, design, construction, and construction engineering.

**Website:** https://idot.illinois.gov/transportation-system/local-transportation-partners/county-engineers-and-local-public-agencies/funding-programs/hsip.html

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="idnr-oslad-program">5. IDNR Open Space Lands Acquisition and Development (OSLAD) Grant</span>

**Description:** The Open Space Lands Acquisition and Development (OSLAD) Program is a state-financed grant program that provides funding assistance to local government agencies for acquisition and/or development of land for public parks and open space. Projects vary from small neighborhood parks or tot lots to large community and county parks and nature areas.

**Website:** https://dnr.illinois.gov/grants/openspacelandsaquisitiondevelopment-grant.html

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="idnr-bicycle-path-program">6. IDNR Bicycle Path Program</span>

**Description:** The Illinois Bicycle Path Grant Program was created in 1990 to financially assist eligible units of government to acquire, construct, and rehabilitate public, non-motorized bicycle paths and directly related support facilities. Grants are available to any local government agency having statutory authority to acquire and develop land for public bicycle path purposes.

**Website:** https://dnr.illinois.gov/grants/bikepathprogram.html

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="usdot-raise-grants-program">7. USDOT Rebuilding American Infrastructure with Sustainability and Equity (RAISE) Grant Program</span>

**Description:** RAISE grants will be awarded for planning or constructing surface transportation infrastructure projects that will improve safety; environmental sustainability; quality of life; mobility and community connectivity; economic competitiveness and opportunity including tourism; state of good repair; partnership and collaboration; and innovation. RAISE was previously known as BUILD and TIGER.

**Website:** https://www.transportation.gov/RAISEgrants

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="illinois-dceo-opportunities">8. Illinois DCEO Opportunities</span>

**Description:** The Illinois Department of Commerce and Economic Opportunity (DCEO) has a variety of grants to assist communities with tourism attraction, food deserts, small business innovation and technology, business attraction, clean energy workforce development, teacher apprenticeship programs, community solar projects, business plans for clean energy contractors, rebuilding downtowns, economic recovery, housing for homeless persons, improving job quality, job training, energy efficiency and renewable energy projects, pregnancy related services, federal grant matching, job creation and retention, disaster response, and broadband development.

**Website:** https://dceo.illinois.gov/aboutdceo/grantopportunities/grants.html

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="ihda-revitalization-and-repair-programs">9. IHDA Revitalization and Repair Programs</span>

**Description:** The Illinois Housing Development Authority (IHDA) has several grant and loan programs to assist with home repairs, accessibility improvements, and addressing vacant residential properties.

**Website:** https://www.ihda.org/my-community/revitalization-programs/

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="infra-program">10. Nationally Significant Multimodal Freight & Highway Projects (INFRA) Program</span>

**Description:** INFRA (the Nationally Significant Multimodal Freight & Highway Projects program) awards competitive grants for multimodal freight and highway projects of national or regional significance to improve the safety, efficiency, and reliability of the movement of freight and people in and across rural and urban areas. This grant is part of the Multimodal Project Discretionary Grant Opportunity (MPDG).

**Website:** https://www.transportation.gov/grants/infra-grant-program

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="protect-discretionary-grant-program">11. Promoting Resilient Operations for Transformative, Efficient, and Cost-Saving Transportation (PROTECT) Discretionary Grant Program</span>

**Description:** The PROTECT Discretionary grant funds projects that address the climate crisis by improving the resilience of the surface transportation system, including highways, public transportation, ports, and intercity passenger rail.

**Website:** https://www.fhwa.dot.gov/environment/protect/discretionary/

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="usdot-fhwa-wildlife-crossings-pilot-program">12. USDOT/FHWA Wildlife Crossings Pilot Program</span>

**Description:** The Wildlife Crossings Pilot Program (WCPP) is a competitive grant program with the goal of reducing Wildlife Vehicle Collisions (WVCs) while improving habitat connectivity for terrestrial and aquatic species.

**Website:** https://highways.dot.gov/federal-lands/wildlife-crossings

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="ccrpc-community-development-assistance-program">13. CCRPC Community Development Assistance Program (CDAP)</span>

**Description:** Loans for businesses and organizations located anywhere in Champaign County.

**Website:** https://ccrpc.org/divisions/planning_and_development/economic_development/financing_programs.php

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="ccrpc-intermediary-relending-program">14. CCRPC Intermediary Relending Program (IRP)</span>

**Description:** Loans for businesses, organizations, and governments in communities of less than 25,000 population in the East Central Illinois counties of Champaign, Douglas, Ford, Iroquois, Piatt, and Vermilion.

**Website:** https://ccrpc.org/divisions/planning_and_development/economic_development/financing_programs.php

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="champaign-county-community-development-corporation-cdc">15. Champaign County Community Development Corporation (CDC)</span>

**Description:** Financing of businesses located anywhere in Champaign County and surrounding communities. Each CDC loan should be matched by member bank financing. Participating member banks include BankChampaign, Busey Bank, Central Illinois Bank, Chase Bank, First Midwest Bank, Midland States Bank, and PNC Bank.

**Website:** https://ccrpc.org/divisions/planning_and_development/economic_development/financing_programs.php

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="champaign-county-enterprise-zones">16. Champaign County Enterprise Zones</span>

**Description:** Enterprise zones are specific geographic areas designed to stimulate economic growth and neighborhood revitalization. The Illinois Enterprise Zone Act was signed into law on December 7, 1982. Businesses and projects locating and expanding in Enterprise Zones may be eligible for exemption on the retailers’ occupation tax paid on building materials, property tax abatement, and various other local and state tax incentives. Programs include Targeted Neighborhood Improvement, Affordable Multifamily Housing, Employment Expansion, Historic Structure Rehabilitation, and Tourism Development. The most common and locally utilized incentives include sales tax exemption on building materials and property tax abatements.

**Website:** https://ccrpc.org/divisions/planning_and_development/economic_development/enterprise_zone.php

----------------

### <span style="color: black; text-decoration: underline red; padding-top: 50px;" id="idot-economic-development-program-iedp">17. IDOT Economic Development Program (IEDP)</span>

**Description:** IEDP provides state assistance for roadway improvements or new construction that are necessary for access to new or expanding industrial, manufacturing, or distribution type companies.

**Website:** https://idot.illinois.gov/transportation-system/local-transportation-partners/county-engineers-and-local-public-agencies/funding-programs/economic-development-program.html

----------------
