---
title: Contact
draft: false
menu: eyebrow
weight: 10
---
For information about CCRPC Grant Services, please contact:

Susan Burgstrom<br>
Planning Manager<br>
Champaign County Regional Planning Commission<br>
sburgstrom@ccrpc.org<br>
217-819-4077